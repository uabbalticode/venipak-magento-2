<?php

namespace Balticode\Venipak\Cron;

use Balticode\Venipak\Command\CommandInterface;
use Balticode\Venipak\Api\PickupPointsProviderInterface;
use Balticode\Venipak\Api\Data\PickupPointsInterface;
use Balticode\Venipak\Model\ResourceModel\PickupPoints\CollectionFactory;

/**
 * Class UpdatePickupPoints
 *
 * @package Balticode\Venipak\Cron
 */
class UpdatePickupPoints
{
    /**
     * @var CommandInterface
     */
    protected $command;

    /**
     * @var PickupPointsProviderInterface
     */
    protected $provider;

    /**
     * @var PickupPointsInterface
     */
    protected $pickupPoints;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var null|int hours how much time leave old records
     */
    protected $lifeTime;

    /**
     * UpdatePickupPoints constructor.
     *
     * @param CommandInterface              $command
     * @param PickupPointsProviderInterface $provider
     * @param PickupPointsInterface         $pickupPoints
     * @param CollectionFactory             $collectionFactory
     * @param null|int                      $lifeTime
     */
    public function __construct(
        CommandInterface $command,
        PickupPointsProviderInterface $provider,
        PickupPointsInterface $pickupPoints,
        CollectionFactory $collectionFactory,
        $lifeTime = null
    ) {
        $this->command = $command;
        $this->provider = $provider;
        $this->pickupPoints = $pickupPoints;
        $this->collectionFactory = $collectionFactory;
        $this->lifeTime = $lifeTime;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $source = $this->command->execute();

        foreach ($source as $row) {
            $this->pickupPoints->setData($row);
            $this->provider->save($this->pickupPoints);
        }

        if ($this->lifeTime !== null) {
            $records = $this->collectionFactory->create();

            $records->getSelect()->where(
                new \Zend_Db_Expr('TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `updated_at`)) >= '
                    . $this->lifeTime * 60 * 60
                )
            );

            $records->walk('delete');
        }
    }
}
