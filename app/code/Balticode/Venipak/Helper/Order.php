<?php

namespace Balticode\Venipak\Helper;

use Magento\Shipping\Helper\Carrier as CarrierHelper;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class Order
 *
 * @package Balticode\Venipak\Helper
 */
class Order extends \Balticode\Venipak\Framework\DataObject
{
    /**
     * @var CarrierHelper
     */
    protected $carrierHelper;

    /**
     * @var OrderInterface
     */
    protected $orderInterface;

    /**
     * Order constructor.
     *
     * @param CarrierHelper  $carrierHelper
     * @param OrderInterface $orderInterface
     * @param array          $data
     */
    public function __construct (
        CarrierHelper $carrierHelper,
        OrderInterface $orderInterface,
        array $data = []
    ) {
        $this->carrierHelper = $carrierHelper;
        $this->orderInterface = $orderInterface;
        parent::__construct($data);
    }

    /**
     * Return How much Packages in Order by default calculation
     *
     * @param $quoteId
     * @return int
     */
    public function countPackagesByQuote($quoteId)
    {
        $order = $this->orderInterface->load($quoteId, 'quote_id');

        return $this->countOrderPackages($order);
    }

    /**
     * Return How much Packages in Order by default calculation
     *
     * @param \Magento\Framework\DataObject $dataObject
     * @return int
     */
    public function countOrderPackages($dataObject)
    {
        if ($dataObject instanceof \Magento\Sales\Model\Order) {
            $order = $dataObject;
        } else {
            $order = $dataObject->getOrder();
        }

        $packCount = 1;
        if ($this->getStoreConfig('pack_type') == 'item') {
            $packCount = (int)$order->getData('total_qty_ordered');
        }

        return $packCount;
    }

    /**
     * Check if quote/order is with my shipping method
     *
     * @param $dataObject
     * @return bool
     */
    public function isMyShippingMethod($dataObject)
    {
        $result = false;
        if ($dataObject) {
            $shippingMethod = $this->getShippingMethod($dataObject);
            $carrier = $this->getCarrier();
            $carrierCode = $carrier->getCarrierCode();

            foreach (array_flip($carrier->getAllMethods()) as $methodCode) {
                if ($shippingMethod == $carrierCode . '_' . $methodCode) {
                    $result = true;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * Check Delivery type is to PickUp Point
     *
     * @param $dataObject
     * @return bool
     */
    public function deliveryInPickupPoint($dataObject)
    {
        $result = false;
        if ($dataObject) {
            $shippingMethod = $this->getShippingMethod($dataObject);
            $carrier = $this->getCarrier();
            $carrierCode = $carrier->getCarrierCode();

            return $shippingMethod == $carrierCode . '_pickup';
        }

        return $result;
    }

    /**
     * Current order payment is COD method
     *
     * @param $dataObject
     * @return bool
     */
    public function isCodPayment($dataObject)
    {
        if ($dataObject instanceof \Magento\Sales\Model\Order) {
            $order = $dataObject;
        } else {
            $order = $dataObject->getOrder();
        }

        $paymentMethod = $order->getPayment()->getMethodInstance()->getCode();

        return in_array($paymentMethod, $this->getCodMethods());
    }

    /**
     * Get Shipping method from object
     *
     * @param $dataObject
     * @return \Magento\Framework\DataObject|string
     */
    public function getShippingMethod($dataObject)
    {
        if ($dataObject instanceof \Magento\Sales\Model\Order) {
            $order = $dataObject;
        } elseif ($dataObject instanceof \Magento\Quote\Model\Quote) {
            $order = $dataObject->getShippingAddress();
        } elseif (is_string($dataObject)) {
            return $dataObject;
        } else {
            $order = $dataObject->getOrder();
        }

        return $order->getShippingMethod();
    }

    /**
     * By order shipping method return carrier method object
     *
     * @param $dataObject
     * @return mixed
     */
    public function getCarrierMethod($dataObject)
    {
        $shippingMethod = $this->getShippingMethod($dataObject);
        $carrier = $this->getCarrier();
        $carrierCode = $carrier->getCarrierCode();

        foreach (array_flip($carrier->getAllMethods()) as $methodCode) {
            if ($shippingMethod == $carrierCode . '_' . $methodCode) {
                return $carrier->getMethod($methodCode);
            }
        }

        return $carrier;
    }

    /**
     * Convert Weight
     *
     * @param        $value
     * @param null   $from
     * @param string $to
     * @return int|null|string
     */
    public function convertMeasureWeight($value, $from = null, $to = \Zend_Measure_Weight::KILOGRAM)
    {
        if ($from == null) {
            $from  = $this->getStoreConfig()->getRawValue(
                'general/locale/weight_unit',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        }

        // @TODO: Find way how to convert correctly
        if ($from == 'kgs') {
            $from = 'KILOGRAM';
        }

        return $this->carrierHelper->convertMeasureWeight($value, strtoupper($from), strtoupper($to));
    }
}
