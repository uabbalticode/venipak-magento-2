<?php

namespace Balticode\Venipak\Helper\Converter;

class VariableType extends Generic
{
    public function toBoolean($data)
    {
        switch (gettype($data)) {
            case 'object':
                $data = (array)$data;
            case 'array':
                $data = !empty(array_filter($data));
            default:
                return (bool)$data;
        }
    }
}
