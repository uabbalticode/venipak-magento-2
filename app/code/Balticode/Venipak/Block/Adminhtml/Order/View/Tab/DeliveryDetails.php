<?php

namespace Balticode\Venipak\Block\Adminhtml\Order\View\Tab;

use Balticode\Venipak\Api\DeliveryDetailsProviderInterface;
use Balticode\Venipak\Helper\Order as OrderHelper;
use Balticode\Venipak\Helper\PickupPoints as PickupPointsHelper;
use Balticode\Venipak\Model\PickupPoints\Management as PickUpPoints;
use Balticode\Venipak\Model\PickupPoints\Provider as PickupPointsProvider;

/**
 * Class DeliveryDetails
 *
 * @package Balticode\Venipak\Block\Adminhtml\Order\View\Tab
 */
class DeliveryDetails extends \Magento\Backend\Block\Template implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'order/view/tab/delivery_details.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    protected $deliveryDetails = null;

    protected $orderHelper;

    protected $pickUpPoints;

    protected $pickUpPointsHelper;

    protected $pickUpPointsProvider;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        DeliveryDetailsProviderInterface $deliveryDetailsInterface,
        OrderHelper $orderHelper,
        PickUpPoints $pickUpPoints,
        PickupPointsProvider $pickUpPointsProvider,
        PickupPointsHelper $pickUpPointsHelper,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->orderHelper = $orderHelper;
        $this->pickUpPoints = $pickUpPoints;
        $this->pickUpPointsProvider = $pickUpPointsProvider;
        $this->pickUpPointsHelper = $pickUpPointsHelper;

        $deliveryDetails = array();
        try {
            $this->deliveryDetails = $deliveryDetailsInterface->getByQuoteId(
                $this->getOrder()->getQuoteId()
            );
            $deliveryDetails = $this->deliveryDetails->getData();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {

        } catch (\Magento\Framework\Exception\LocalizedException $exception) {

        }

        $data = array_merge($data, $deliveryDetails);

        parent::__construct($context, $data);
    }

    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('current_order');
    }

    private function getQuoteId()
    {
        return $this->getOrder()->getQuoteId();
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return 'Venipak';
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return 'Venipak';
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return $this->orderHelper->isMyShippingMethod($this->getOrder());
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Get Tab Class
     *
     * @return string
     */
    public function getTabClass()
    {
        // I wanted mine to load via AJAX when it's selected
        // That's what this does
        return 'ajax only';
    }

    /**
     * Get Class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->getTabClass();
    }

    /**
     * Get Tab Url
     *
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('venipak/*/deliveryDetailsTab', ['_current' => true]);
    }

    public function getOrderIncrementId()
    {
        return $this->getOrder()->getIncrementId();
    }

    public function getOrderId()
    {
        return $this->getOrder()->getId();
    }

    public function getTimeStampHtml()
    {
        $column = 'time_stamp';
        return $this->getSelectHtml($column, $this->getDeliveryTimeStamp()->toOptionArray(), $this->getData($column));
    }

    public function getPacksHtml()
    {
        $packCount = 0;
        if ($this->deliveryDetails) {
            $packCount = $this->deliveryDetails->getPackCount();
        }
        return $this->getInputHtml(
            'pack_count',
            $packCount
        );
    }

    public function getWarehouseHtml()
    {
        $column = 'warehouse_id';

        return $this->getSelectHtml(
            $column,
            $this->getWarehouse()->toOptionArray(),
            !empty ($this->getData($column) ) ? $this->getData($column) : '_0_0'
        );
    }

    public function getPickUpDetails()
    {
        $pickUpData = $this->getData('pickup_point_data');

        if ($pickUpData) {
            $pickUpData = new \Balticode\Venipak\Framework\DataObject((array)json_decode($pickUpData));
        } else {
            $pickupPointId = $this->getData('pickup_point_id');
            $pickUpData = $this->pickUpPointsProvider->getByPointId($pickupPointId);
        }

        $data = [];

        if ($pickUpData) {
            $data['name'] = '<b>'.$pickUpData->getName().'</b>';
            $data['city'] = $pickUpData->getCity();
            $data['address'] = $pickUpData->getAddress();
            $data['country'] = $pickUpData->getCountry() . '-' . $pickUpData->getZip();
            $data['contact_t'] = $pickUpData->getContactT();
            $data['description'] = $pickUpData->getDescription();

            if ($workingHours = $pickUpData->getWeekWorkingHours()) {
                $data['empty_line'] = '&nbsp;';
                $data['working_hours'] = implode('<br />', $workingHours);
            }

            $data = array_filter($data);
        }

        return implode('<br />', $data);
    }

    public function getPickUpHtml()
    {
        if ($this->editable()) {
            $pickUpPoints = $this->pickUpPoints->fetchOrder($this->getOrderId());
            $pickUpPoints = $this->pickUpPointsHelper->groupByCity($pickUpPoints);

            $list = [];
            foreach ($pickUpPoints as $city => $data) {
                foreach ($data['terminals'] as $terminal) {
                    $list[$city][] = [
                        'value' => $terminal['id'],
                        'label' => $terminal['name'] . ' ' . $terminal['address']
                    ];
                }
            }

            $column = 'pickup_point_id';

            $html = $this->getSelectHtmlWithOptGroup($column, $list, $this->getData($column));
        } else {
            $pickUpPoint = json_decode($this->getData('pickup_point_data'));

            $format = '<span>%s</span>';
            $content = $pickUpPoint->name . ' ' . $pickUpPoint->address;

            $html = sprintf($format, $content);
        }

        return $html;
    }

    public function deliveryInPickupPoint()
    {
        return $this->orderHelper->deliveryInPickupPoint($this->getOrder());
    }

    public function getOfficeNoHtml()
    {
        $column = 'office_no';
        return $this->getInputHtml($column, $this->getData($column));
    }

    public function getDoorNoHtml()
    {
        $column = 'door_no';
        return $this->getInputHtml($column, $this->getData($column));
    }

    public function getWarehouseNoHtml()
    {
        $column = 'client_warehouse';
        return $this->getInputHtml($column, $this->getData($column));
    }

    public function editable()
    {
        return !$this->getData('sent');
    }

    public function getYesNoHtml($column)
    {
        return $this->getSelectHtml(
            $column,
            $this->getYesNo()->toOptionArray(),
            !empty ($this->getData($column) ) ? $this->getData($column) : 0
        );
    }

    protected function getSelectHtmlWithOptGroup($name, $optionsWithGroups, $selected = null)
    {
        if ($this->editable()) {
            $groupsFormat = '<optgroup label="%s">%s</optgroup>';
            $optionFormat = '<option value="%s" %s>%s</option>';
            $content = '';
            foreach ($optionsWithGroups as $group => $options) {
                $optionsContent = '';
                foreach ($options as $data) {
                    $optionsContent .= sprintf($optionFormat,
                        $data['value'],
                        (($data['value'] == $selected) ? 'selected="selected"' : ''),
                        $data['label']
                    );
                }

                if ($optionsContent) {
                    $content .= sprintf($groupsFormat,
                        $group,
                        $optionsContent
                    );
                }
            }

            $format = '<select name="%s">%s</select>';
        } else {
            $format = '<span name="%s">%s</span>';
            $content = $this->getData('pickup_point_data');
        }

        $html = sprintf($format, $name, $content);

        return $html;
    }

    /**
     * @param      $name
     * @param      $options
     * @param null $selected
     * @return string
     */
    protected function getSelectHtml($name, $options, $selected = null)
    {
        $format = '<option value="%s" %s>%s</option>';
        $content = '';
        foreach ($options as $option) {
            $value = $option['value'];
            $label = $option['label'];
            $isSelected = $value == $selected;

            if ($isSelected && !$this->editable()) break;

            $content .= sprintf($format,
                $value,
                ($isSelected ? 'selected="selected"' : ''),
                $label
            );
        }

        $format = '<select name="%s">%s</select>';

        if (! $this->editable()) {
            $format = '<span name="%s">%s</span>';
            $content = $label;
        }

        $html = sprintf($format, $name, $content);

        return $html;
    }

    protected function getInputHtml($name, $value)
    {
        if ($this->editable()) {
            $format = '<input name="%s" value="%s" />';

            $html = sprintf($format, $name, $value);
        } else {
            $html = $value;
        }

        return $html;
    }
}
