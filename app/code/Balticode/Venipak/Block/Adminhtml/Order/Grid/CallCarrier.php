<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Balticode\Venipak\Block\Adminhtml\Order\Grid;

use Magento\Backend\Block\Template\Context;

/**
 * Class CallCarrier
 *
 * @package Balticode\Venipak\Block\Adminhtml\Order\Grid
 */
class CallCarrier extends \Magento\Backend\Block\Template
{
    /**
     * @var string
     */
    protected $_template = 'Balticode_Venipak::order/grid/call_carrier.phtml';

    /**
     * CallCarrier constructor.
     *
     * @param Context $context
     * @param array   $data
     */
    public function __construct (
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}
