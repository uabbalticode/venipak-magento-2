<?php

namespace Balticode\Venipak\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class Map extends AbstractFieldArray
{
    /**
     * Enable the "Add after" button or not
     *
     * @var bool
     */
    protected $_addAfter = false;

    protected $canBeEmpty = true;

    protected $collectedColumns = [];

    protected $currentColumnType = 'text';

    /**
     * Source model factory
     *
     * @var \Magento\Config\Model\Config\SourceFactory
     */
    protected $sourceFactory;

    /**
     * @var string
     */
    protected $_template = 'Balticode_Venipak::system/config/form/field/array.phtml';

    public function __construct (
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Config\Model\Config\SourceFactory $sourceFactory,
        array $data = []
    ) {
        $this->sourceFactory = $sourceFactory;
        parent::__construct($context, $data);
    }

    /**
     * @codingStandardsIgnoreStart
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _prepareToRender()
    {
        $sourceModel = $this->getElement()->getFieldConfig('source_model');
        $this->canBeEmpty = $this->getElement()->getFieldConfig('can_be_empty') ?? true;
        $validate = $this->getElement()->getFieldConfig('validate');
        $value = $this->getElement()->getValue();
        if (!is_array($value) && !$this->canBeEmpty) {
            $value = $this->unSerialize($value);
            $this->getElement()->setValue($value);
        }

        $parentConfig = [];
        if ($validate) {
            $parentConfig['class'] = $validate;
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $columns = $objectManager->create($sourceModel);

        foreach ($columns->collectOptionValues() as $column) {
            $this->currentColumnType = $column['type'];
                $this->addColumn($column['value'], array_merge($column, $parentConfig));

            if ($this->currentColumnType == 'select') {
                $columnRender = &$this->_columns[$column['value']]['renderer'];
                if ($columnRender instanceof \Magento\Framework\View\Element\AbstractBlock) {

                    $columnRender->setClass($column['class'] ?? '');
                    $columnRender->setStyle($column['style'] ?? '');
                    $columnRender->setTitle($column['label']);
                    $columnRender->setName($this->_getCellInputElementName($column['value']));
                    $columnRender->setId($this->_getCellInputElementId('<%- _id %>', $column['value']));

                    $columnRender->setOptions($this->getOptions($column));
                }
            }
        }

        $this->collectedColumns[$sourceModel] = $this->getColumns();
    }

    protected function unSerialize($value)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $serializer = $objectManager->create(\Magento\Framework\Serialize\Serializer\Json::class);
        return $serializer->unserialize($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     * @throws \Exception
     */
    protected function _toHtml()
    {
        $sourceModel = $this->getElement()->getFieldConfig('source_model');
        if (array_key_exists($sourceModel, $this->collectedColumns)) {
            $this->_columns = $this->collectedColumns[$sourceModel];
        } else {
            // Remove columns from before rendering
            $this->_columns = [];
            $this->_prepareToRender();
        }
        if (empty($this->_columns)) {
            throw new \Exception('At least one column must be defined.');
        }
        return parent::_toHtml();
    }

    /**
     * Retrieve static options or source model option list
     *
     * @return array
     */
    public function getOptions($data)
    {

        if (isset($data['source_model'])) {
            $sourceModel = $data['source_model'];
            $optionArray = $this->_getOptionsFromSourceModel($sourceModel);
            return $optionArray;
        } elseif (isset($data['options']) && isset($data['options']['option'])) {
            $options = $data['options']['option'];
            $options = $this->_getStaticOptions($options);
            return $options;
        }
        return [];
    }

    /**
     * Get Static Options list
     *
     * @param array $options
     * @return array
     */
    protected function _getStaticOptions(array $options)
    {
        foreach (array_keys($options) as $key) {
            $options[$key]['label'] = __($options[$key]['label']);
            $options[$key]['value'] = $this->_fillInConstantPlaceholders($options[$key]['value']);
        }
        return $options;
    }

    /**
     * Takes a string and searches for placeholders ({{CONSTANT_NAME}}) to replace with a constant value.
     *
     * @param string $value an option value that may contain a placeholder for a constant value
     * @return mixed|string the value after being replaced by the constant if needed
     */
    private function _fillInConstantPlaceholders($value)
    {
        if (is_string($value) && preg_match('/^{{(\\\\[A-Z][\\\\A-Za-z\d_]+::[A-Z\d_]+)}}$/', $value, $matches)) {
            $value = constant($matches[1]);
        }
        return $value;
    }

    /**
     * Retrieve options list from source model
     *
     * @param string $sourceModel Source model class name or class::method
     * @return array
     */
    protected function _getOptionsFromSourceModel($sourceModel)
    {
        $method = false;
        if (preg_match('/^([^:]+?)::([^:]+?)$/', $sourceModel, $matches)) {
            array_shift($matches);
            list($sourceModel, $method) = array_values($matches);
        }

        $sourceModel = $this->sourceFactory->create($sourceModel);
        if ($sourceModel instanceof \Magento\Framework\DataObject) {
            $sourceModel->setPath($this->getPath());
        }
        if ($method) {
            if ($this->currentColumnType == 'multiselect') {
                $optionArray = $sourceModel->{$method}();
            } else {
                $optionArray = [];
                foreach ($sourceModel->{$method}() as $key => $value) {
                    if (is_array($value)) {
                        $optionArray[] = $value;
                    } else {
                        $optionArray[] = ['label' => $value, 'value' => $key];
                    }
                }
            }
        } else {
            $optionArray = $sourceModel->toOptionArray($this->currentColumnType == 'multiselect');
        }
        return $optionArray;
    }
}
