<?php

namespace Balticode\Venipak\Observer;

use Balticode\Venipak\Helper\Order as OrderHelper;
use Balticode\Venipak\Model\Carrier;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\PaymentException;

/**
 * Class CheckPayment
 *
 * @package Balticode\Venipak\Observer
 */
class CheckPayment implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var OrderHelper
     */
    protected $orderHelper;

    /**
     * @var Carrier
     */
    protected $carrier;

    /**
     * CheckPayment constructor.
     *
     * @param OrderHelper $orderHelper
     * @param Carrier     $carrier
     */
    public function __construct (
        OrderHelper $orderHelper,
        Carrier $carrier
    ) {
        $this->orderHelper = $orderHelper;
        $this->carrier = $carrier;
    }

    /**
     * Check Payment Method
     *
     * @param Observer $observer
     * @throws PaymentException
     */
    public function execute (Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();
        if ($this->orderHelper->isMyShippingMethod($quote)) {
            $payment = $quote->getPayment()->getMethodInstance();
            $carrier = $this->orderHelper->getCarrierMethod($quote);
            if (!$carrier->isPaymentAvailable($payment)) {
               throw new PaymentException(__('Sorry, but payment is not allowed with selected carrier method.'));
            }
        }
    }
}
