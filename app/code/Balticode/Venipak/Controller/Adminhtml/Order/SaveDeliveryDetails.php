<?php

namespace Balticode\Venipak\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;
use Balticode\Venipak\Api\DeliveryDetailsProviderInterface as DeliveryDetailsProvider;
use Balticode\Venipak\Api\Data\DeliveryDetailsInterface as DeliveryDetails;

/**
 * Class SaveDeliveryDetails
 *
 * @package Balticode\Venipak\Controller\Adminhtml\Order
 */
class SaveDeliveryDetails extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Balticode\Venipak\Api\DeliveryDetailsProviderInterface
     */
    protected $deliveryDetailsProvider;

    protected $deliveryDetails;

    protected $coreRegistry;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param OrderManagementInterface $orderManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger,
        DeliveryDetailsProvider $deliveryDetailsProvider,
        DeliveryDetails $deliveryDetails
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->deliveryDetailsProvider = $deliveryDetailsProvider;
        $this->coreRegistry = $coreRegistry;
        $this->deliveryDetails = $deliveryDetails;
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $translateInline,
            $resultPageFactory,
            $resultJsonFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $orderManagement,
            $orderRepository,
            $logger
        );
    }

    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('current_order');
    }

    /**
     * Generate order history for ajax request
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        $this->_initOrder();

        $result = [];
        try {
            $quoteId = $this->getOrder()->getQuoteId();
            $deliveryDetails = $this->deliveryDetailsProvider->getByQuoteId($quoteId);
            $result = $deliveryDetails->addData($this->getRequest()->getParams())->save();
            $result['error'] = false;
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $result['error'] = true;
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($result);
    }
}
