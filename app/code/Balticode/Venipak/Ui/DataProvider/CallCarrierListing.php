<?php

namespace Balticode\Venipak\Ui\DataProvider;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class CallCarrierListing
 *
 * @package Balticode\Venipak\Ui\DataProvider
 */
class CallCarrierListing extends DataProvider
{
    /**
     * @var Json
     */
    protected $serializer;

    /**
     * CallCarrierListing constructor.
     *
     * @param string                $name
     * @param string                $primaryFieldName
     * @param string                $requestFieldName
     * @param ReportingInterface    $reporting
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param RequestInterface      $request
     * @param FilterBuilder         $filterBuilder
     * @param Json                  $serializer
     * @param array                 $meta
     * @param array                 $data
     */
    public function __construct (
         string $name,
         string $primaryFieldName,
         string $requestFieldName,
         ReportingInterface $reporting,
         SearchCriteriaBuilder $searchCriteriaBuilder,
         RequestInterface $request,
         FilterBuilder $filterBuilder,
         Json $serializer,
         array $meta = [],
         array $data = []
    ) {
        $this->serializer = $serializer;
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $reporting,
            $searchCriteriaBuilder,
            $request,
            $filterBuilder,
            $meta,
            $data
        );

        $dataTime = new \DateTime();
        $this->addFilter(
            $this->filterBuilder->setField('pickup_date')
                ->setValue($dataTime->format('Y-m-d'))
                ->setConditionType('eq')
                ->create()
        );
    }

    /**
     * @param SearchResultInterface $searchResult
     * @return array
     */
    protected function searchResultToOutput(SearchResultInterface $searchResult)
    {
        $arrItems = [];

        $arrItems['items'] = [];
        foreach ($searchResult->getItems() as $item) {
            $itemData = [];
            foreach ($item->getCustomAttributes() as $attribute) {
                $itemData[$attribute->getAttributeCode()] = $attribute->getValue();
            }
            $this->mergeValues($itemData);
            $arrItems['items'][] = $itemData;
        }

        $arrItems['totalRecords'] = $searchResult->getTotalCount();

        return $arrItems;
    }

    /**
     * Merge params
     * @param $itemData
     */
    protected function mergeValues(&$itemData)
    {
        $itemData['weight_volume'] = implode(', ', [
            $itemData['weight'] ? $itemData['weight'].'kg' : '-',
            $itemData['volume'] ? $itemData['volume'].'m3' : '-'
        ]);

        try {
            $warehouseData = $this->serializer->unserialize($itemData['warehouse_address']);
        } catch (\InvalidArgumentException $e) {
            $warehouseData = [];
        }
        $itemData['address'] = implode(', ', array_filter([
            $warehouseData['city'] ?? null,
            $warehouseData['address'] ?? null,
            $warehouseData['post_code'] ?? null,
            $warehouseData['country_iso'] ?? null
        ]));

        $itemData['contacts'] = implode(', ', array_filter([
            $warehouseData['contact_name'] ?? null,
            $warehouseData['contact_number'] ?? null,
        ]));

        $itemData['delivery_time'] = $itemData['pickup_time_from'].'-'.$itemData['pickup_time_to'];
    }
}
