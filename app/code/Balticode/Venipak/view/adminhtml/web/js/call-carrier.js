require(
    [
        'jquery',
        'Magento_Ui/js/modal/modal',
        'uiRegistry',
        'mage/translate'
    ],
    function(
        $,
        modal,
        registry
    ) {
        var options = {
            type: 'slide',
            title: $.mage.__('Call Venipak Courier'),
            buttons: [{
                text: $.mage.__('Cancel'),
                class: '',
                click: function () {
                    this.closeModal();
                }
            },
            {
                text: $.mage.__('Call'),
                class: 'primary',
                click: function () {
                    registry.get('call_venipak_carrier_form.call_venipak_carrier_form').save();
                }
            }]
        };

        var slideModal = modal(options, $('#call_venipak_carrier'));
        registry.set('call_venipak_carrier_form.call_venipak_carrier_form_modal', slideModal);
    }
);
