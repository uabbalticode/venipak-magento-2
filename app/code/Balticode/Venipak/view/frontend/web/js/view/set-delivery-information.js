/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-save-processor',
        'Balticode_Venipak/js/model/set-carrier-information',
        'Balticode_Venipak/js/model/set-pickup-information'
    ],
function (
    Component,
    shippingSaveProcessor,
    venipakCarrierShippingSaveProcessor,
    venipakPickupShippingSaveProcessor,
) {
    'use strict';

    /** Register save venipak delivery details processor */
    shippingSaveProcessor.registerProcessor('venipak-carrier', venipakCarrierShippingSaveProcessor);
    shippingSaveProcessor.registerProcessor('venipak-pickup', venipakPickupShippingSaveProcessor);

    /** Add view logic here if needed */
    return Component.extend({});
});
