define([
    'ko',
    'underscore'
], function (
    ko,
    _
) {
    'use strict';

    var terminals = ko.observableArray([]);
    var terminalListIsReady = ko.observable(false);

    return {
        terminals: terminals,
        terminalListIsReady: terminalListIsReady,
        isLoading: ko.observable(false),

        init: function (terminalList) {
            this.groupByCity(terminalList);
            terminalListIsReady(true);
        },

        getTerminalList: function () {
            return terminals();
        },

        groupByCity: function(terminalList) {
            var objectKeys = Object.keys(terminalList);
            var groups = {};
            for (var i = 0; i < objectKeys.length; i++) {
                var key = objectKeys[i];
                var city = terminalList[key].city;
                if (!groups[city]) {
                    groups[city] = {name: city, terminals: []};
                }
                groups[city]['terminals'].push(terminalList[key]);
            }

            terminals(_.values(groups).sort(function(a,b){
                return a.name.localeCompare(b.name);
            }));
        }
    };
});
