<?php

namespace Balticode\Venipak\Model;

use Magento\Framework\Model\AbstractModel;
use Balticode\Venipak\Api\Data\PickupPointsInterface;
use Balticode\Venipak\Model\ResourceModel\PickupPoints as PickupPointsResource;

/**
 * Class PickupPoints
 *
 * @package Balticode\Venipak\Model
 */
class PickupPoints extends AbstractModel implements PickupPointsInterface
{

    /** @var  array  */
    private $pickupPoints;

    /**
     * Init resource model
     *
     * @return void
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(PickupPointsResource::class);
        // @codingStandardsIgnoreEnd
    }

    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    public function setId($value)
    {
        return $this->setData(self::ENTITY_ID, $value);
    }

    /**
     * @return mixed
     */
    public function getPickupPointId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @return mixed
     */
    public function setPickupPointId($value)
    {
        return $this->setData(self::ID, $value);
    }

    public function getName()
    {
        return $this->getData(self::NAME);
    }

    public function setName($value)
    {
        return $this->setData(self::NAME, $value);
    }

    public function getCode()
    {
        return $this->getData(self::CODE);
    }

    public function setCode($value)
    {
        return $this->setData(self::CODE, $value);
    }

    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    public function setAddress($value)
    {
        return $this->setData(self::ADDRESS, $value);
    }

    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    public function setCity($value)
    {
        return $this->setData(self::CITY, $value);
    }

    public function getZip()
    {
        return $this->getData(self::ZIP);
    }

    public function setZip($value)
    {
        return $this->setData(self::ZIP, $value);
    }

    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    public function setCountry($value)
    {
        return $this->setData(self::COUNTRY, $value);
    }

    public function getTerminal()
    {
        return $this->getData(self::TERMINAL);
    }

    public function setTerminal($value)
    {
        return $this->setData(self::TERMINAL, $value);
    }

    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    public function setDescription($value)
    {
        return $this->setData(self::DESCRIPTION, $value);
    }

    public function getWorkingHours()
    {
        return $this->getData(self::WORKING_HOURS);
    }

    public function setWorkingHours($value)
    {
        return $this->setData(self::WORKING_HOURS, $value);
    }

    public function getLat()
    {
        return $this->getData(self::CONTACT_NO);
    }

    public function setLat($value)
    {
        return $this->setData(self::CONTACT_NO, $value);
    }

    public function getLng()
    {
        return $this->getData(self::LAT);
    }

    public function setLng($value)
    {
        return $this->setData(self::LAT, $value);
    }

    public function getPickUpEnabled()
    {
        return $this->getData(self::LANG);
    }

    public function setPickUpEnabled($value)
    {
        return $this->setData(self::LANG, $value);
    }

    public function getCodEnabled()
    {
        return $this->getData(self::PICKUP_ENABLED);
    }

    public function setCodEnabled($value)
    {
        return $this->setData(self::PICKUP_ENABLED, $value);
    }

    public function getLdgEnabled()
    {
        return $this->getData(self::COD_ENABLED);
    }

    public function setLdgEnabled($value)
    {
        return $this->setData(self::COD_ENABLED, $value);
    }

    public function getSizeLimit()
    {
        return $this->getData(self::LDG_ENABLES);
    }

    public function setSizeLimit($value)
    {
        return $this->setData(self::LDG_ENABLES, $value);
    }

    public function getWeekWorkingHours()
    {
        $result = [];
        $workHours = json_decode($this->getWorkingHours());
        if ($workHours) {
            $days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $getLocale = $objectManager->get('Magento\Framework\Locale\Resolver');
            $haystack  = $getLocale->getLocale();
            $lang = strstr($haystack, '_', true);

            try {
                $localeData = \Zend_Locale_Data::getList($lang, 'days');
                $localeData = $localeData['format']['narrow'];

                foreach ($workHours as $dayNumber => $hours) {
                    $result[] = $localeData[$days[$dayNumber]] . '. ' .
                        $hours->from_h . ':' . str_pad($hours->from_m, 2, 0, STR_PAD_LEFT) . '-' .
                        $hours->to_h . ':' . str_pad($hours->to_m, 2, 0, STR_PAD_LEFT);
                }
            } catch (\Zend_Locale_Exception $exception) {

            }
        }

        return $result;
    }
}
