<?php

namespace Balticode\Venipak\Model\PickupPoints;

use Balticode\Venipak\Api\PickupPointsManagementInterface;
use Balticode\Venipak\Api\PickupPointsProviderInterface;
use Balticode\Venipak\Api\Data\PickupPointsInterface;
use Balticode\Venipak\Helper\Order as OrderHelper;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\SortOrder;
use Magento\Sales\Api\OrderRepositoryInterface;

class Management implements PickupPointsManagementInterface
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var \Balticode\Venipak\Api\PickupPointsProviderInterface
     */
    private $terminalProvider;

    /**
     * @var \Magento\Quote\Api\CartItemRepositoryInterface
     */
    private $cartItemRepository;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Quote\Api\GuestCartItemRepositoryInterface $cartItemRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        PickupPointsProviderInterface $terminalProviderInterface,
        OrderHelper $orderHelper,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->cartItemRepository = $cartItemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->terminalProvider = $terminalProviderInterface;
        $this->orderHelper = $orderHelper;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param string $cartId
     * @param string $countryId
     * @return \Balticode\Venipak\Api\Data\PickupPointsInterface[]
     */
    public function fetch($cartId, $countryId)
    {
        $itemList = $this->cartItemRepository->getList($cartId);

        $weight = 0;
        foreach ($itemList as $item) {
            $weight += ($item->getWeight() * $item->getQty());
        }

        // Convert weight into kilograms
        $weight = $this->orderHelper->convertMeasureWeight($weight);

        $sortOrder = $this->sortOrderBuilder->setField('id')->setDirection(SortOrder::SORT_ASC)->create();
        $citySortOrder = $this->sortOrderBuilder->setField('city')->setDirection(SortOrder::SORT_ASC)->create();
        $this->searchCriteriaBuilder->addFilter(PickupPointsInterface::SIZE_LIMIT, $weight, 'gteq');
        if ($countryId) {
            $this->searchCriteriaBuilder->addFilter(PickupPointsInterface::COUNTRY, $countryId);
        }
        /** @var \Balticode\Venipak\Api\Data\PickupPointsSearchResultsInterface $searchResult */
        $searchResult = $this->terminalProvider->getList(
            $this->searchCriteriaBuilder->setSortOrders([$sortOrder, $citySortOrder])->create()
        );

        return $searchResult->getItems();
    }

    /**
     * @param string $cartId
     * @param string $countryId
     * @return \Balticode\Venipak\Api\Data\PickupPointsInterface[]
     */
    public function fetchCustomer($cartId, $countryId)
    {
        $quoteRepository = $this->getCartRepository();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $quoteRepository->getActive($cartId);
        $itemList = $quote->getItems();

        $weight = 0;
        foreach ($itemList as $item) {
            $weight += ($item->getWeight() * $item->getQty());
        }

        // Convert weight into kilograms
        $weight = $this->orderHelper->convertMeasureWeight($weight);

        $sortOrder = $this->sortOrderBuilder->setField('id')->setDirection(SortOrder::SORT_ASC)->create();
        $citySortOrder = $this->sortOrderBuilder->setField('city')->setDirection(SortOrder::SORT_ASC)->create();
        $this->searchCriteriaBuilder->addFilter(PickupPointsInterface::SIZE_LIMIT, $weight, 'gteq');
        if ($countryId) {
            $this->searchCriteriaBuilder->addFilter(PickupPointsInterface::COUNTRY, $countryId);
        }
        /** @var \Balticode\Venipak\Api\Data\PickupPointsSearchResultsInterface $searchResult */
        $searchResult = $this->terminalProvider->getList(
            $this->searchCriteriaBuilder->setSortOrders([$sortOrder, $citySortOrder])->create()
        );

        return $searchResult->getItems();
    }

    /**
     * Find terminals for order
     *
     * @param string $orderId
     * @return \Balticode\Venipak\Api\Data\PickupPointsInterface[]
     */
    public function fetchOrder($orderId)
    {
        $order = $this->orderRepository->get($orderId);
        $countryId = $order->getShippingAddress()->getCountryId();

        $weight = $order->getWeight();
        // Convert weight into kilograms
        // $weight = $this->orderHelper->convertMeasureWeight($weight);

        $sortOrder = $this->sortOrderBuilder->setField('id')->setDirection(SortOrder::SORT_ASC)->create();
        $citySortOrder = $this->sortOrderBuilder->setField('city')->setDirection(SortOrder::SORT_ASC)->create();
        $this->searchCriteriaBuilder->addFilter(PickupPointsInterface::SIZE_LIMIT, $weight, 'gteq');
        if ($countryId) {
            $this->searchCriteriaBuilder->addFilter(PickupPointsInterface::COUNTRY, $countryId);
        }
        /** @var \Balticode\Venipak\Api\Data\PickupPointsSearchResultsInterface $searchResult */
        $searchResult = $this->terminalProvider->getList(
            $this->searchCriteriaBuilder->setSortOrders([$sortOrder, $citySortOrder])->create()
        );

        return $searchResult->getItems();
    }

    /**
     * Get Cart repository
     *
     * @return \Magento\Quote\Api\CartRepositoryInterface
     * @deprecated 100.2.0
     */
    private function getCartRepository()
    {
        if (!$this->cartRepository) {
            $this->cartRepository = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Quote\Api\CartRepositoryInterface::class);
        }
        return $this->cartRepository;
    }
}
