<?php

namespace Balticode\Venipak\Model;

use Magento\Framework\Model\AbstractModel;
use Balticode\Venipak\Api\Data\CarrierDataInterface;
use Balticode\Venipak\Model\ResourceModel\CarrierData as CarrierDataResource;

/**
 * Class CarrierData
 *
 * @package Balticode\Venipak\Model
 */
class CarrierData extends AbstractModel implements CarrierDataInterface
{
    /**
     * Init resource model
     *
     * @return void
     * @codingStandardsIgnoreStart
     */
    protected function _construct ()
    {
        $this->_init(CarrierDataResource::class);
        // @codingStandardsIgnoreEnd
    }

    /**
     * @return mixed|void
     */
    public function getId()
    {
        $this->getData(self::ENTITY_ID);
    }

    /**
     * @param mixed $value
     * @return AbstractModel|mixed|void
     */
    public function setId($value)
    {
        $this->setData(self::ENTITY_ID, $value);
    }

    /**
     * @return mixed|void
     */
    public function getWeight()
    {
        $this->getData(self::WEIGHT);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setWeight($value)
    {
        $this->setData(self::WEIGHT, $value);
    }

    /**
     * @return mixed|void
     */
    public function getVolume()
    {
        $this->getData(self::VOLUME);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setVolume($value)
    {
        $this->setData(self::VOLUME, $value);
    }

    /**
     * @return mixed|void
     */
    public function getPickupDate()
    {
        $this->getData(self::PICKUP_DATE);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setPickupDate($value)
    {
        $this->setData(self::PICKUP_DATE, $value);
    }

    /**
     * @return mixed|void
     */
    public function getPickupTimeFrom()
    {
        $this->getData(self::PICKUP_TIME_FROM);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setPickupTimeFrom($value)
    {
        $this->setData(self::PICKUP_TIME_FROM, $value);
    }

    /**
     * @return mixed|void
     */
    public function getPickupTimeTo()
    {
        $this->getData(self::PICKUP_TINE_TO);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setPickupTimeTo($value)
    {
        $this->setData(self::PICKUP_TINE_TO, $value);
    }

    /**
     * @return mixed|void
     */
    public function getWarehouseId()
    {
        $this->getData(self::WAREHOUSE_ID);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setWarehouseId($value)
    {
        $this->setData(self::WAREHOUSE_ID, $value);
    }

    /**
     * @return mixed|void
     */
    public function getWarehouseAddress()
    {
        $this->getData(self::WAREHOUSE_ADDRESS);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setWarehouseAddress($value)
    {
        $this->setData(self::WAREHOUSE_ADDRESS, $value);
    }

    /**
     * @return mixed|void
     */
    public function getSpp()
    {
        $this->getData(self::SPP);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setSpp($value)
    {
        $this->setData(self::SPP, $value);
    }

    /**
     * @return mixed|void
     */
    public function getDocNo()
    {
        $this->getData(self::DOC_NO);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setDocNo($value)
    {
        $this->setData(self::DOC_NO, $value);
    }

    /**
     * @return mixed|void
     */
    public function getComment()
    {
        $this->getData(self::COMMENT);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setComment($value)
    {
        $this->setData(self::COMMENT, $value);
    }

    /**
     * @return mixed|void
     */
    public function getCode()
    {
        $this->getData(self::CODE);
    }

    /**
     * @param $value
     * @return mixed|void
     */
    public function setCode($value)
    {
        $this->setData(self::CODE, $value);
    }

    /**
     * @return mixed|void
     */
    public function getCreatedAt()
    {
        $this->getData(self::CREATED_AT);
    }

    /**
     * @return mixed|void
     */
    public function getUpdatedAt()
    {
        $this->getData(self::UPDATED_AT);
    }
}