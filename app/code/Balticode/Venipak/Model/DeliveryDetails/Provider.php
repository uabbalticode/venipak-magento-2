<?php

namespace Balticode\Venipak\Model\DeliveryDetails;

use Balticode\Venipak\Api\DeliveryDetailsProviderInterface;
use Balticode\Venipak\Model\DeliveryDetailsFactory;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Balticode\Venipak\Api\Data\DeliveryDetailsInterface;
use Balticode\Venipak\Model\ResourceModel\DeliveryDetails as DeliveryDetailsResource;

class Provider implements DeliveryDetailsProviderInterface
{
    private $deliveryDetailsFactory;

    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var DeliveryDetailsFactory
     */
    protected $externalAddressFactory;

    /**
     * @var DeliveryDetailsResource
     */
    protected $resource;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $cartRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        DeliveryDetailsResource $resource,
        DeliveryDetailsFactory $deliveryDetailsFactory
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;

        $this->resource = $resource;
        $this->deliveryDetailsFactory = $deliveryDetailsFactory;
    }

    /**
     * @inheritdoc
     * @throws CouldNotSaveException
     */
    public function save(
        $cartId,
        DeliveryDetailsInterface $deliveryDetails
    ) {
        /** @var $quoteIdMask \Magento\Quote\Model\QuoteIdMask */
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        $quoteId = $quoteIdMask->getQuoteId();
        if ($quoteId) {
            $deliveryDetails->setQuoteId($quoteId);
        }

        // @TODO: Check another way check about existing row
        try {
            $id = $this->getByQuoteId($quoteId)->getId();
            $deliveryDetails->setId($id);
        } catch (NoSuchEntityException $e) {

        }

        try {
            $this->resource->save($deliveryDetails);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the delivery details: %1',
                $exception->getMessage()
            ));
        }
        return $deliveryDetails;
    }

    /**
     * Save Delivery Details
     *
     * @param string                   $cartId
     * @param \Balticode\Venipak\Api\Data\DeliveryDetailsInterface $deliveryDetails
     * @return \Balticode\Venipak\Api\Data\DeliveryDetailsInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function saveCustomer(
        $cartId,
        DeliveryDetailsInterface $deliveryDetails
    ) {
        $quoteRepository = $this->getCartRepository();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $quoteRepository->getActive($cartId);
        $quoteId = $quote->getId();

        if ($quoteId) {
            $deliveryDetails->setQuoteId($quoteId);
        }

        // @TODO: Check another way check about existing row
        try {
            $id = $this->getByQuoteId($quoteId)->getId();
            $deliveryDetails->setId($id);
        } catch (NoSuchEntityException $e) {

        }

        try {
            $this->resource->save($deliveryDetails);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the delivery details: %1',
                $exception->getMessage()
            ));
        }
        return $deliveryDetails;
    }

    /**
     * @param string $quoteId
     * @return \Balticode\Venipak\Api\Data\DeliveryDetailsInterface
     * @throws NoSuchEntityException
     */
    public function getByQuoteId($quoteId)
    {
        $deliveryDetails = $this->deliveryDetailsFactory->create();
        $this->resource->load($deliveryDetails, $quoteId, DeliveryDetailsInterface::QUOTE_ID);
        if (!$deliveryDetails->getId()) {
            throw new NoSuchEntityException(__('Delivery details for quote ID="%1" does not exist.', $quoteId));
        }
        return $deliveryDetails;
    }

    /**
     * @inheritdoc
     */
    public function delete(DeliveryDetailsInterface $deliveryDetails)
    {
        try {
            if (!$deliveryDetails->getId()) {
                throw new NoSuchEntityException(__('Delivery details does not exist.'));
            }
            $this->resource->delete($deliveryDetails);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the delivery details: %1',
                $exception->getMessage()
            ));
        }
    }

    /**
     * Get Cart repository
     *
     * @return \Magento\Quote\Api\CartRepositoryInterface
     * @deprecated 100.2.0
     */
    private function getCartRepository()
    {
        if (!$this->cartRepository) {
            $this->cartRepository = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Quote\Api\CartRepositoryInterface::class);
        }
        return $this->cartRepository;
    }
}
