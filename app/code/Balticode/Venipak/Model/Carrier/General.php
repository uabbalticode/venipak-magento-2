<?php

namespace Balticode\Venipak\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Balticode\Venipak\Framework\DataObject;

/**
 * Class General
 *
 * @package Balticode\Venipak\Model\Carrier
 * @method getOpportunity()     array parameter from DI
 * @method getForbiddenMethods()
 */
class General extends DataObject
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $rateMethodFactory;

    public function __construct (
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->rateMethodFactory = $rateMethodFactory;
        parent::__construct($data);
    }

    /**
     * Get Carrier Rate data
     *
     * @param RateRequest $request
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Method
     */
    public function getRateMethod(RateRequest $request)
    {
        $price = $this->getPrice($request);

        if ($price === false) {
            return false;
        }

        $method = $this->rateMethodFactory->create();
        $method->setMethod($this->getMethod());
        $method->setMethodTitle($this->getMethodTitle());
        $method->setPrice($price);
        $method->setCost($price);

        return $method;
    }

    /**
     * Check Method is Available
     *
     * @param RateRequest $request
     * @return bool
     */
    public function isAvailable(RateRequest $request)
    {
        $available = true;
        $opportunity = $this->getOpportunity();
        if ($opportunity) {
            foreach ($opportunity as $condition) {
                $method = key($condition);
                if (method_exists($this, $method)) {
                    $parameters = reset($condition);
                    $parameters = new DataObject($parameters);
                    $available = call_user_func_array([$this, $method], [$request, $parameters]);
                    // If one of checks fails do not need check seconds
                    if (!$available) break;
                }
            }
        }

        return $available;
    }

    /**
     * Check Payment is available
     *
     * @param \Magento\Payment\Model\MethodInterface $payment
     * @return bool
     */
    public function isPaymentAvailable(\Magento\Payment\Model\MethodInterface $payment)
    {
        $paymentCode = $payment->getCode();
        return !in_array($paymentCode, (array)$this->getForbiddenMethods());
    }

    /**
     * Check Method is Available by country
     *
     * @param RateRequest $request
     * @param array       $parameters
     * @return bool
     */
    protected function checkAvailableShipCountries(RateRequest $request, $parameters = [])
    {
        $available = true;
        $allowSpecific = $parameters->getSallowspecific();
        if ($allowSpecific) {
            $countryList = $parameters->getSpecificcountry();
            $countryList = explode(',', $countryList);
            $available = in_array($request->getDestCountryId(), $countryList);
        }

        return $available;
    }

    /**
     * Check Method is Available by Pattern in Product Attribute
     *
     * @param RateRequest $request
     * @param array       $parameters
     * @return bool
     */
    protected function checkByItemAttribute(RateRequest $request, $parameters = [])
    {
        $pattern = $parameters->getValue();
        // Get all attributes where need to search
        $attributes = (array)$parameters->getAttribute();
        // Check each quote items
        foreach ($request->getAllItems() as $item) {
            // Reload product because in quote is not full product
            $product = $item->getProduct()->load($item->getProduct()->getId());
            // Check by each product attribute
            foreach ($attributes as $attribute) {
                // Take product attribute value
                $value = $product->getData($attribute);
                if (strpos($value, $pattern) !== false) {
                    return false;
                }
            }
        }

        return true;
    }
}
