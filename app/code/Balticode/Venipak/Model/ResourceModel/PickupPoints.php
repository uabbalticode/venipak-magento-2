<?php

namespace Balticode\Venipak\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Balticode\Venipak\Api\Data\PickupPointsInterface;

/**
 * Class PickupPoints
 *
 * @package Balticode\Venipak\Model\ResourceModel
 */
class PickupPoints extends AbstractDb
{
    /**
     * Define resource model
     *
     * @return void
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init('venipak_pickup_points', PickupPointsInterface::ENTITY_ID);
        // @codingStandardsIgnoreEnd
    }
}
