<?php

namespace Balticode\Venipak\Model\ResourceModel\PickupPoints;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Balticode\Venipak\Api\Data\PickupPointsInterface;

/**
 * Class Collection
 *
 * @package Balticode\Venipak\Model\ResourceModel\PickupPoints
 */
class Collection extends AbstractCollection
{
    /**
     * @codingStandardsIgnoreStart
     * @var string
     */
    protected $_idFieldName = PickupPointsInterface::ENTITY_ID;

    /**
     * Define resource model
     *
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct()
    {
        $this->_init('Balticode\Venipak\Model\PickupPoints', 'Balticode\Venipak\Model\ResourceModel\PickupPoints');
        // @codingStandardsIgnoreEnd
    }
}
