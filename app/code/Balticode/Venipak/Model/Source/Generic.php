<?php

namespace Balticode\Venipak\Model\Source;

use Magento\Shipping\Model\Carrier\Source\GenericInterface;
use Balticode\Venipak\Model\Carrier;

/**
 * Generic source
 */
class Generic extends \Balticode\Venipak\Framework\DataObject implements GenericInterface
{
    /**
     * @var \Balticode\Venipak\Model\Carrier
     */
    protected $shippingVenipak;

    protected $self = false;

    /**
     * Generic constructor.
     *
     * @param \Balticode\Venipak\Model\Carrier $shippingVenipak
     * @param array   $data
     */
    public function __construct(
        Carrier $shippingVenipak,
        array $data = []
    ) {
        $this->shippingVenipak = $shippingVenipak;

        parent::__construct(array_merge($this->loadOptionValues(), $data));
    }

    /**
     * Returns array to be used in multi select on back-end
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $this->self = true;
        $source = $this->collectOptionValues();
        if ($source) {
            foreach ($source as $line => $title) {
                if (is_array($title)
                    && array_key_exists('value', $title)
                    && array_key_exists('label', $title)
                ) {
                    $line = $title['value'];
                    $title = $title['label'];
                }

                $options[] = ['value' => $line, 'label' => __($title)];
            }
        }
        return $options;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toListArray()
    {
        $options = [];
        $this->self = true;
        $source = $this->collectOptionValues();
        if ($source) {
            foreach ($source as $line => $title) {
                if (is_array($title)
                    && array_key_exists('value', $title)
                    && array_key_exists('label', $title)
                ) {
                    $line = $title['value'];
                    $title = $title['label'];
                }

                $options[$line] = __($title);
            }
        }

        return $options;
    }

    /**
     * Collecting parameters
     *
     * @return array|mixed|null
     */
    public function collectOptionValues()
    {
        if (($this->_data['method'] ?? false)
            && method_exists($this->shippingVenipak, $this->_data['method'])
        ) {
            $codes = call_user_func([$this->shippingVenipak, $this->_data['method']]);
        } else {
            $codes = $this->execute();
        }

        if ($codes instanceof \Balticode\Venipak\Framework\DataObject) {
            $codes = call_user_func([$codes, $this->_data['method']]);
        }

        return $codes;
    }

    /**
     * After load Option Values save to this Object
     *
     * @return array
     */
    public function loadOptionValues()
    {
        $collectedData = $this->collectOptionValues();
        return ['data' => $collectedData];
    }
}
