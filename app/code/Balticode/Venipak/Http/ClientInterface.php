<?php

namespace Balticode\Venipak\Http;

/**
 * Interface ClientInterface
 *
 * @package Balticode\Venipak\Http
 */
interface ClientInterface
{
    /**
     * Places request to gateway. Returns result as ENV array
     *
     * @param \Balticode\Venipak\Http\TransferInterface $transferObject
     * @return array
     */
    public function placeRequest(\Balticode\Venipak\Http\TransferInterface $transferObject);
}
