<?php

namespace Balticode\Venipak\Http\Converter\Zend;

use Balticode\Venipak\Http\ConverterInterface;
use Magento\Framework\Xml\Parser;

/**
 * Class ImportSendConverter
 *
 * @package Balticode\Venipak\Http\Converter\Zend
 */
class ImportSendConverter implements ConverterInterface
{
    /**
     * @var Parser
     */
    private $xmlParser;

    /**
     * ImportSendConverter constructor.
     *
     * @param Parser $xmlParser
     */
    public function __construct(
        Parser $xmlParser
    ) {
        $this->xmlParser = $xmlParser;
    }

    /**
     * Converts gateway response to ENV structure
     *
     * @param mixed $response
     *
     * @return array
     */
    public function convert($response)
    {
        $this->xmlParser->loadXML($response);
        $resultResponse = $this->xmlParser->xmlToArray();

        return $resultResponse;
    }
}
