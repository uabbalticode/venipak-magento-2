<?php

namespace Balticode\Venipak\Http\Builder;

use Balticode\Venipak\Http\BuilderInterface;
use Balticode\Venipak\Model\DeliveryDetails\Provider as DeliveryDetails;
use Magento\Framework\Exception\NoSuchEntityException;
use Balticode\Venipak\Helper\Order as OrderHelper;
use Balticode\Venipak\Command\CommandInterface;

/**
 * Class PrintLabelBuilder
 *
 * @package Balticode\Venipak\Http\Builder
 */
class PrintLabelBuilder implements BuilderInterface
{
    protected $store_config;

    protected $deliveryDetails;

    protected $orderHelper;

    protected $command;

    public function __construct (
        \ArrayAccess $store_config,
        DeliveryDetails $deliveryDetails,
        OrderHelper $orderHelper,
        CommandInterface $command
    ) {
        $this->store_config = $store_config;
        $this->deliveryDetails = $deliveryDetails;
        $this->orderHelper = $orderHelper;
        $this->command = $command;
    }

    public function build (array $buildSubject)
    {
        $packNo = [];
        foreach ($buildSubject as $order) {
            if ($this->orderHelper->isMyShippingMethod($order)) {
                try {
                    $packNumber = $this->deliveryDetails->getByQuoteId($order->getQuoteId())->getPackNumber();
                    if (!$packNumber) {
                        $packNumber = $this->command->execute(['order' => $order]);
                    } else {
                        $packNumber = explode(',', $packNumber);
                    }

                    $packNo = array_merge($packNo, $packNumber);
                } catch (NoSuchEntityException $exception) {
                    throw $exception;
                }
            }
        };

        $packNo = array_filter($packNo);

        // Remove space between numbers
        $packNo = explode(',', str_replace(' ', '', implode(',', $packNo)));

        $result = [
            'user' => $this->store_config->getUsername(),
            'pass' => $this->store_config->getPassword(),
            'pack_no' => $packNo,
            'type' => $this->store_config->getLabelSize(),
            'sandbox' => $this->store_config->getSandbox(),
        ];

        return $result;
    }
}
