<?php

namespace Balticode\Venipak\Http\Builder\CallCarrier;

use Balticode\Venipak\Http\BuilderInterface;
use Balticode\Venipak\Framework\DataObject;
use Balticode\Venipak\Model\DeliveryDetails\Provider as DeliveryDetails;
use Balticode\Venipak\Helper\Order as OrderHelper;
use Balticode\Venipak\Api\PickupPointsProviderInterface as PickupPoints;
use Magento\Framework\Exception\ConfigurationMismatchException;

/**
 * Class Builder
 *
 * @package Balticode\Venipak\Http\Builder\CallCarrier
 */
class Builder implements BuilderInterface
{
    /**
     * @var \Traversable
     */
    protected $converter;

    /**
     * @var DataObject
     */
    protected $dataObject;

    /**
     * @var DeliveryDetails
     */
    protected $deliveryDetails;

    /**
     * @var OrderHelper
     */
    protected $orderHelper;

    /**
     * @var \ArrayAccess
     */
    protected $storeConfig;

    /**
     * @var PickupPoints
     */
    protected $pickupPoints;

    /**
     * Builder constructor.
     *
     * @param \Traversable    $converter
     * @param \ArrayAccess    $storeConfig
     * @param DataObject      $dataObject
     * @param DeliveryDetails $deliveryDetails
     * @param PickupPoints    $pickupPoints
     * @param OrderHelper     $orderHelper
     */
    public function __construct (
        \Traversable $converter,
        \ArrayAccess $storeConfig,
        DataObject $dataObject,
        DeliveryDetails $deliveryDetails,
        PickupPoints $pickupPoints,
        OrderHelper $orderHelper
    ) {
        $this->converter = clone $converter;
        $this->dataObject = $dataObject;
        $this->deliveryDetails = $deliveryDetails;
        $this->orderHelper = $orderHelper;
        $this->storeConfig = $storeConfig;
        $this->pickupPoints = $pickupPoints;
    }

    /**
     * @param array $buildSubject
     * @return array
     * @throws ConfigurationMismatchException
     */
    public function build (array $buildSubject)
    {
        $this->converter->addAttribute('type', 3);

        if ($this->storeConfig->getSendConsignorData()) {
            $this->addConsignor($shipment, $this->storeConfig);
        }

        $this->addSender($this->converter, $this->storeConfig->getWarehouse($buildSubject['warehouse_id']));

        $pickupData = strtotime($buildSubject['pickup_date']);

        $this->converter->addChild('weight', $buildSubject['weight']);
        $this->converter->addChild('volume', $buildSubject['volume']);
        $this->converter->addChild('date_y', date( "Y", $pickupData));
        $this->converter->addChild('date_m', date( "m", $pickupData));
        $this->converter->addChild('date_d', date( "d", $pickupData));
        $this->converter->addChild('hour_from', $buildSubject['pickup_time_from_h']);
        $this->converter->addChild('min_from', $buildSubject['pickup_time_from_m']);
        $this->converter->addChild('hour_to', $buildSubject['pickup_time_to_h']);
        $this->converter->addChild('min_to', $buildSubject['pickup_time_to_m']);
        $this->converter->addChild('comment', $buildSubject['comment']);
        $this->converter->addChild('spp', $buildSubject['spp']);
        $this->converter->addChild('doc_no', $buildSubject['doc_no']);

        return $this->converter->asNiceXml();
    }

    /**
     * @param $parentElement
     * @param $data
     * @throws ConfigurationMismatchException
     */
    protected function addSender(&$parentElement, $data)
    {
        $countryCode = strtoupper($data['country_iso']);
        if (!in_array($countryCode, $this->storeConfig->getAvailableCountryCode())) {
            throw new ConfigurationMismatchException(
                new \Magento\Framework\Phrase(
                    'Warehouse country code not allowed: %1. Available: %2',
                    [
                        $countryCode,
                        implode(', ', $this->storeConfig->getAvailableCountryCode())
                    ]
                )
            );
        }

        $warehouseData = [
            'name' => $data['name'],
            'company_code' => $data['company_code'],
            'country' => $countryCode,
            'city' => $data['city'],
            'address' => $data['address'],
            'post_code' => preg_replace('/[^0-9]/', '', $data['post_code']),
            'contact_person' => $data['contact_name'],
            'contact_tel' => $data['contact_number'],
        ];
        $children = $parentElement->addChild('sender');
        $this->addContactBlock($children, $this->dataObject->setData($warehouseData));
    }

    /**
     * @param $parentElement
     * @param $data
     * @return bool
     */
    protected function addConsignor(&$parentElement, $data)
    {
        $consignorData = [
            'name' => $data->getConsignorName(),
            'company_code' => $data->getConsignorCompanyCode(),
            'country' => $data->getConsignorCountry(),
            'city' => $data->getConsignorCity(),
            'address' => $data->getConsignorAddress(),
            'post_code' => $data->getConsignorPostCode(),
            'contact_person' => $data->getConsignorPerson(),
            'contact_tel' => $data->getConsignorTel(),
            'contact_email' => $data->getConsignorEmail()
        ];
        // If we not have any data inserted
        if (count(array_filter($consignorData)) == 0) {
            return false;
        }
        $children = $parentElement->addChild('consignor');
        $this->addContactBlock($children, $this->dataObject->setData($consignorData));
    }

    protected function addContactBlock(&$element, $data)
    {
        $element->addChild('name', $data->getName());
        $element->addChild('company_code', $data->getCompanyCode());
        $element->addChild('country', $data->getCountry());
        $element->addChild('city', $data->getCity());
        $element->addChild('address', $data->getAddress());
        $element->addChild('post_code', $data->getPostCode());
        $element->addChild('contact_person', $data->getContactPerson());
        $element->addChild('contact_tel', $data->getContactTel());
        $element->addChild('contact_email', $data->getContactEmail());
    }
}
