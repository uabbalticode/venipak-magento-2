<?php

namespace Balticode\Venipak\Http\Builder;

use Balticode\Venipak\Http\BuilderInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class CallCarrierBuilder
 *
 * @package Balticode\Venipak\Http\Builder
 */
class CallCarrierBuilder implements BuilderInterface
{
    /**
     * @var \ArrayAccess
     */
    protected $store_config;

    /**
     * @var BuilderInterface
     */
    protected $builder;

    /**
     * CallCarrierBuilder constructor.
     *
     * @param \ArrayAccess     $store_config
     * @param BuilderInterface $builder
     */
    public function __construct (
        \ArrayAccess $store_config,
        BuilderInterface $builder
    ) {
        $this->store_config = $store_config;
        $this->builder = $builder;
    }

    /**
     * @param array $buildSubject
     * @return array
     */
    public function build (array $buildSubject)
    {
        $result = [
            'user' => $this->store_config->getUsername(),
            'pass' => $this->store_config->getPassword(),
            'xml_text' => $this->builder->build($buildSubject),
            'sandbox' => $this->store_config->getSandbox(),
        ];

        return $result;
    }

    /**
     * Data about what we collect to send XML
     *
     * @return DataObject
     */
    public function getBuilderPattern()
    {
        return $this->builder->getTransferData();
    }
}
