<?php

namespace Balticode\Venipak\Http\Builder\ImportSend;

use Balticode\Venipak\Http\BuilderInterface;
use Balticode\Venipak\Framework\DataObject;
use Balticode\Venipak\Model\DeliveryDetails\Provider as DeliveryDetails;
use Balticode\Venipak\Helper\Order as OrderHelper;
use Balticode\Venipak\Api\PickupPointsProviderInterface as PickupPoints;
use Magento\Framework\Exception\ConfigurationMismatchException;
use Magento\Framework\Simplexml\Element as SimpleXml;

/**
 * Class Builder
 *
 * @package Balticode\Venipak\Http\Builder\ImportSend
 */
class Builder implements BuilderInterface
{
    protected $converter;

    protected $dataObject;

    protected $deliveryDetails;

    protected $orderHelper;

    protected $storeConfig;

    protected $pickupPoints;

    protected $transferData;

    public function __construct (
        \ArrayAccess $storeConfig,
        DataObject $dataObject,
        DeliveryDetails $deliveryDetails,
        PickupPoints $pickupPoints,
        OrderHelper $orderHelper
    ) {
        $this->converter = new SimpleXml('<description/>');
        $this->dataObject = $dataObject;
        $this->transferData = clone $dataObject;
        $this->deliveryDetails = $deliveryDetails;
        $this->orderHelper = $orderHelper;
        $this->storeConfig = $storeConfig;
        $this->pickupPoints = $pickupPoints;
    }

    public function build (array $buildSubject)
    {
        $order = $buildSubject['order'];
        $deliveryType = 1;

        $deliveryDetails = $this->deliveryDetails->getByQuoteId($order->getQuoteId());
        $warehouseId = $deliveryDetails->getWarehouseId();
        $packageCount = $deliveryDetails->getPackCount();

        $manifestNumber = $this->storeConfig->getManifestNumber($warehouseId);

        // Save Data to Object
        $this->transferData->setData('warehouse_id', $warehouseId);
        $this->transferData->setData('delivery_type', $deliveryType);
        $this->transferData->setData('manifest_no', $manifestNumber);
        $this->transferData->setData('package_count', $packageCount);

        // Create XML
        $this->converter->addAttribute('type', $deliveryType);
        $manifest = $this->converter->addChild('manifest');
        $manifest->addAttribute('title', $manifestNumber);
        $shipment = $manifest->addChild('shipment');

        if ($this->storeConfig->getSendConsignorData()) {
            $this->addConsignor($shipment, $this->storeConfig);
        }

        $deliveryMethod = $this->orderHelper->getCarrierMethod($order)->getMethod();

        if ($deliveryMethod == 'carrier') {
            $this->addPersonConsignee($shipment, $order->getShippingAddress());
        } elseif ($deliveryMethod == 'pickup') {
            $pickupPointId = $deliveryDetails->getPickupPointId();
            // @TODO: try catch exception if point not exist anymore
            $pickupPoint = $this->pickupPoints->getByPointId($pickupPointId);
            $this->addPickupPointConsignee($shipment, $pickupPoint, $order->getShippingAddress());
        }

        $this->addSender($shipment, $this->storeConfig->getWarehouse($warehouseId));

        $attribute = $shipment->addChild('attribute');
        $attribute->addChild('shipment_code');
        $attribute->addChild('delivery_type', $deliveryDetails->getTimeStamp());
        $attribute->addChild('return_doc', $deliveryDetails->getReturnDoc());
        $attribute->addChild('dlr_code'); // Paysera Code
        $attribute->addChild('doc_no', $order->getIncrementId());
        $attribute->addChild('cod', $this->orderHelper->isCodPayment($order) ? $order->getBaseGrandTotal() : 0);
        $attribute->addChild('cod_type', $order->getOrderCurrencyCode());
        $attribute->addChild('comment_door_code', $deliveryDetails->getDoorNo());
        $attribute->addChild('comment_office_no', $deliveryDetails->getOfficeNo());
        $attribute->addChild('comment_warehous_no', $deliveryDetails->getClientWarehouse());
        $attribute->addChild('comment_call', $deliveryDetails->getDeliveryCall());

        if ($packageCount == $order->getTotalItemCount()) {
            foreach ($order->getAllVisibleItems() as $item) {
                $weight = $item->getWeight() ? $item->getWeight() * $item->getQtyOrdered() : null;
                // Convert to Kg
                $weight = $this->orderHelper->convertMeasureWeight($weight);

                $pack = $shipment->addChild('pack');
                $pack->addChild('pack_no', $this->storeConfig->getPackNr());
                $pack->addChild('doc_no', $item->getSku());
                $pack->addChild('weight', $weight ? $weight : 1 );
                $pack->addChild('volume', $item->getVolume());
            }
        } else {
            $weight = 0;
            foreach ($order->getAllItems() as $item) {
                $weight += ($item->getWeight() * $item->getQty()) ;
            }

            // Convert to Kg
            $weight = $this->orderHelper->convertMeasureWeight($weight);
            $weight = $weight / $packageCount;
            for (; $packageCount > 0; $packageCount--) {
                $pack = $shipment->addChild('pack');
                $pack->addChild('pack_no', $this->storeConfig->getPackNr());
                $pack->addChild('doc_no');
                $pack->addChild('weight', $weight);
                $pack->addChild('volume');
            }
        }

        // Set to clean old system config values (pack number is increment)
        $this->storeConfig->clean();

        return $this->converter->asNiceXml();
    }

    protected function addPersonConsignee(&$parentElement, $data)
    {
        $countryCode = strtoupper($data->getCountryId());

        if (!in_array($countryCode, $this->storeConfig->getAvailableCountryCode())) {
            throw new ConfigurationMismatchException(
                new \Magento\Framework\Phrase(
                    'Shipping country code not allowed: %1. Available: %2',
                    [
                        $countryCode,
                        implode(', ', $this->storeConfig->getAvailableCountryCode())
                    ]
                )
            );
        }

        $shippingData = [
            'name' => $data->getFirstname() . ' ' . $data->getLastname(),
            'company_code' => $data->getCompanyCode(),
            'country' => $countryCode,
            'city' => $data->getCity(),
            'address' => implode(' ', (array)$data->getStreet()),
            'post_code' => preg_replace('/[^0-9]/', '', $data->getPostcode()),
            'contact_person' => $data->getFirstname(),
            'contact_tel' => $data->getTelephone(),
            'contact_email' => $data->getEmail()
        ];
        $children = $parentElement->addChild('consignee');
        $this->addContactBlock($children, $this->dataObject->setData($shippingData));
    }

    protected function addPickupPointConsignee(&$parentElement, $data, $orderData)
    {
        $shippingData = [
            'name' => $data->getName(),
            'company_code' => $data->getCode(),
            'country' => $data->getCountry(),
            'city' => $data->getCity(),
            'address' => $data->getAddress(),
            'post_code' => $data->getZip(),
            'contact_person' => $orderData->getFirstname(),
            'contact_tel' => $orderData->getTelephone(),
            'contact_email' => $orderData->getEmail(),
        ];
        $children = $parentElement->addChild('consignee');
        $this->addContactBlock($children, $this->dataObject->setData($shippingData));
    }

    protected function addSender(&$parentElement, $data)
    {
        $countryCode = strtoupper($data['country_iso']);
        if (!in_array($countryCode, $this->storeConfig->getAvailableCountryCode())) {
            throw new ConfigurationMismatchException(
                new \Magento\Framework\Phrase(
                    'Warehouse country code not allowed: %1. Available: %2',
                    [
                        $countryCode,
                        implode(', ', $this->storeConfig->getAvailableCountryCode())
                    ]
                )
            );
        }

        $warehouseData = [
            'name' => $data['name'],
            'company_code' => $data['company_code'],
            'country' => $countryCode,
            'city' => $data['city'],
            'address' => $data['address'],
            'post_code' => preg_replace('/[^0-9]/', '', $data['post_code']),
            'contact_person' => $data['contact_name'],
            'contact_tel' => $data['contact_number'],
        ];
        $children = $parentElement->addChild('sender');
        $this->addContactBlock($children, $this->dataObject->setData($warehouseData));
    }

    protected function addConsignor(&$parentElement, $data)
    {
        $consignorData = [
            'name' => $data->getConsignorName(),
            'company_code' => $data->getConsignorCompanyCode(),
            'country' => $data->getConsignorCountry(),
            'city' => $data->getConsignorCity(),
            'address' => $data->getConsignorAddress(),
            'post_code' => $data->getConsignorPostCode(),
            'contact_person' => $data->getConsignorPerson(),
            'contact_tel' => $data->getConsignorTel(),
            'contact_email' => $data->getConsignorEmail()
        ];
        // If we not have any data inserted
        if (count(array_filter($consignorData)) == 0) {
            return false;
        }
        $children = $parentElement->addChild('consignor');
        $this->addContactBlock($children, $this->dataObject->setData($consignorData));
    }

    protected function addContactBlock(&$element, $data)
    {
        $element->addChild('name', $data->getName());
        $element->addChild('company_code', $data->getCompanyCode());
        $element->addChild('country', $data->getCountry());
        $element->addChild('city', $data->getCity());
        $element->addChild('address', $data->getAddress());
        $element->addChild('post_code', $data->getPostCode());
        $element->addChild('contact_person', $data->getContactPerson());
        $element->addChild('contact_tel', $data->getContactTel());
        $element->addChild('contact_email', $data->getContactEmail());
    }

    /**
     * Data about what we collect to send XML
     *
     * @return DataObject
     */
    public function getTransferData()
    {
        return $this->transferData;
    }
}
