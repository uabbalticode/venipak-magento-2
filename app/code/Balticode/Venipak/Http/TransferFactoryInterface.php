<?php

namespace Balticode\Venipak\Http;

/**
 * Interface TransferFactoryInterface
 *
 * @package Balticode\Venipak\Http
 */
interface TransferFactoryInterface
{
    const URL_TARGET_PREFIX = 'ws';

    /**
     * Builds gateway transfer object
     *
     * @param array $request
     * @return \Balticode\Venipak\Http\TransferInterface
     */
    public function create(array $request);
}
