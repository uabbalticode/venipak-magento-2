<?php

namespace Balticode\Venipak\Http\Zend;

use Balticode\Venipak\Helper\StoreConfig;
use Balticode\Venipak\Http\Transfer\TransferBuilder;
use Balticode\Venipak\Http\TransferFactoryInterface;

/**
 * Class CallCarrierTransferFactory
 *
 * @package Balticode\Venipak\Http\Zend
 */
class CallCarrierTransferFactory implements TransferFactoryInterface
{
    const URL_PATH = 'import/send.php';

    /**
     * @var StoreConfig
     */
    private $storeConfig;

    /**
     * @var TransferBuilder
     */
    private $transferBuilder;

    public function __construct(
        StoreConfig $storeConfig,
        TransferBuilder $transferBuilder
    ) {
        $this->storeConfig = $storeConfig;
        $this->transferBuilder = $transferBuilder;
    }

    /**
     * @param array $request
     * @return \Balticode\Venipak\Http\Transfer\Transfer|\Balticode\Venipak\Http\TransferInterface
     */
    public function create(array $request)
    {
        $this->transferBuilder
            ->setBody($request)
            ->setMethod(\Zend_Http_Client::POST)
            ->setUri(
                implode(
                    '/',
                    array_filter([
                        rtrim($this->storeConfig->getEndpointUrl(), '/'),
                        self::URL_PATH
                    ])
                )
            );

        return $this->transferBuilder->build();
    }
}
