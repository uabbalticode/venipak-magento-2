<?php

namespace Balticode\Venipak\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface DeliveryDetailsInterface
 *
 * @package Balticode\Venipak\Api\Data
 */
interface DeliveryDetailsInterface extends ExtensibleDataInterface
{
    const ENTITY_ID = 'id';
    const QUOTE_ID = 'quote_id';
    const LABEL_SIZE = 'label_size';
    const PACK_COUNT = 'pack_count';
    const WAREHOUSE_ID = 'warehouse_id';
    const CLIENT_WAREHOUSE = 'client_warehouse';
    const RETURN_DOC = 'return_doc';
    const MANIFEST_NO = 'manifest_no';
    const PACK_NO = 'pack_no';
    const TIME_STAMP = 'time_stamp';
    const OFFICE_NO = 'office_no';
    const DOOR_NO = 'door_no';
    const DELIVERY_CALL = 'delivery_call';
    const SENT = 'sent';
    const PICKUP_POINT_ID = 'pickup_point_id';
    const PICKUP_POINT_DATA = 'pickup_point_data';

    /**
     * Get id
     * @return int
     */
    public function getId();

    /**
     * Get quote_id
     * @return string|null
     */
    public function getQuoteId();

    /**
     * Set quote_id
     * @param string $quoteId
     * @return \Balticode\Venipak\Api\Data\DeliveryDetailsInterface
     */
    public function setQuoteId($quoteId);

    /**
     * @return string
     */
    public function getLabelSize();

    /**
     * @param string $value
     * @return mixed
     */
    public function setLabelSize($value);

    /**
     * @return string
     */
    public function getPackCount();

    /**
     * @param string $value
     * @return mixed
     */
    public function setPackCount($value);

    /**
     * @return string
     */
    public function getWarehouseId();

    /**
     * @param string $value
     * @return mixed
     */
    public function setWarehouseId($value);

    /**
     * @return string
     */
    public function getWarehouseNumber();

    /**
     * @param string $value
     * @return mixed
     */
    public function setWarehouseNumber($value);

    /**
     * @return string
     */
    public function getReturnDoc();

    /**
     * @param string $value
     * @return mixed
     */
    public function setReturnDoc($value);

    /**
     * @return string
     */
    public function getManifestNumber();

    /**
     * @param string $value
     * @return mixed
     */
    public function setManifestNumber($value);

    /**
     * @return string
     */
    public function getPackNumber();

    /**
     * @param string $value
     * @return mixed
     */
    public function setPackNumber($value);

    /**
     * @return string
     */
    public function getDeliveryTime();

    /**
     * @param string $value
     * @return mixed
     */
    public function setDeliveryTime($value);

    /**
     * @return string
     */
    public function getOfficeNumber();

    /**
     * @param string $value
     * @return mixed
     */
    public function setOfficeNumber($value);

    /**
     * @return string
     */
    public function getDoorCode();

    /**
     * @param string $value
     * @return mixed
     */
    public function setDoorCode($value);

    /**
     * @return boolean
     */
    public function getNeedCall();

    /**
     * @param boolean $value
     * @return mixed
     */
    public function setNeedCall($value);

    /**
     * @return string
     */
    public function getSent();

    /**
     * @param string $value
     * @return mixed
     */
    public function setSent($value);

    /**
     * @return string
     */
    public function getPickupPointId();

    /**
     * @param string $value
     * @return mixed
     */
    public function setPickupPointId($value);

    /**
     * @return string
     */
    public function getPickupPointData();

    /**
     * @param string $value
     * @return mixed
     */
    public function setPickupPointData($value);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Balticode\Venipak\Api\Data\DeliveryDetailsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Balticode\Venipak\Api\Data\DeliveryDetailsExtensionInterface $deliveryDetails
     * @return void
     */
    public function setExtensionAttributes
    (
        \Balticode\Venipak\Api\Data\DeliveryDetailsExtensionInterface $deliveryDetails
    );
}
