<?php

namespace Balticode\Venipak\Api\Data;

/**
 * Interface CarrierDataInterface
 *
 * @package Balticode\Venipak\Api\Data
 */
interface CarrierDataInterface
{
    const ENTITY_ID = 'id';
    const WEIGHT = 'weight';
    const VOLUME = 'volume';
    const PICKUP_DATE = 'pickup_date';
    const PICKUP_TIME_FROM = 'pickup_time_from';
    const PICKUP_TINE_TO = 'pickup_time_to';
    const WAREHOUSE_ID = 'warehouse_id';
    const WAREHOUSE_ADDRESS = 'warehouse_address';
    const SPP = 'spp';
    const DOC_NO = 'doc_no';
    const COMMENT = 'comment';
    const CODE = 'code';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param $value
     * @return mixed
     */
    public function setId($value);

    /**
     * @return mixed
     */
    public function getWeight();

    /**
     * @param $value
     * @return mixed
     */
    public function setWeight($value);

    /**
     * @return mixed
     */
    public function getVolume();

    /**
     * @param $value
     * @return mixed
     */
    public function setVolume($value);

    /**
     * @return mixed
     */
    public function getPickupDate();

    /**
     * @param $value
     * @return mixed
     */
    public function setPickupDate($value);

    /**
     * @return mixed
     */
    public function getPickupTimeFrom();

    /**
     * @param $value
     * @return mixed
     */
    public function setPickupTimeFrom($value);

    /**
     * @return mixed
     */
    public function getPickupTimeTo();

    /**
     * @param $value
     * @return mixed
     */
    public function setPickupTimeTo($value);

    /**
     * @return mixed
     */
    public function getWarehouseId();

    /**
     * @param $value
     * @return mixed
     */
    public function setWarehouseId($value);

    /**
     * @return mixed
     */
    public function getWarehouseAddress();

    /**
     * @param $value
     * @return mixed
     */
    public function setWarehouseAddress($value);

    /**
     * @return mixed
     */
    public function getSpp();

    /**
     * @param $value
     * @return mixed
     */
    public function setSpp($value);

    /**
     * @return mixed
     */
    public function getDocNo();

    /**
     * @param $value
     * @return mixed
     */
    public function setDocNo($value);

    /**
     * @return mixed
     */
    public function getComment();

    /**
     * @param $value
     * @return mixed
     */
    public function setComment($value);

    /**
     * @return mixed
     */
    public function getCode();

    /**
     * @param $value
     * @return mixed
     */
    public function setCode($value);

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @return mixed
     */
    public function getUpdatedAt();
}
