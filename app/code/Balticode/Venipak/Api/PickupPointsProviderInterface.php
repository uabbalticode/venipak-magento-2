<?php

namespace Balticode\Venipak\Api;

use Balticode\Venipak\Api\Data\PickupPointsInterface;

/**
 * Interface PickupPointsProviderInterface
 *
 * @package Balticode\Venipak\Api
 */
interface PickupPointsProviderInterface
{
    /**
     * Save Pickup Point
     *
     * @param \Balticode\Venipak\Api\Data\PickupPointsInterface $pickupPoints
     * @return \Balticode\Venipak\Api\Data\PickupPointsInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(PickupPointsInterface $pickupPoints);

    /**
     * Retrieve Pickup Points Data by id
     *
     * @param string $entityId
     * @return \Balticode\Venipak\Api\Data\PickupPointsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByPointId($entityId);

    /**
     * Delete Pickup Point
     *
     * @param \Balticode\Venipak\Api\Data\PickupPointsInterface $pickupPoints
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(PickupPointsInterface $pickupPoints);

    /**
     * Get Pickup Points list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface
     * @return \Balticode\Venipak\Api\Data\PickupPointsSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
